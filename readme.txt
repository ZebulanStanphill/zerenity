=== Zerenity ===
Contributors: Zebulan Stanphill
Requires at least: 5.4
Requires PHP: 7.2.0
Tested up to: 5.4
Stable tag: 0.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

A Twenty Twenty fork that strives to strike a balance between options and opinions. Optimized for modern browsers.

== Description ==

A Twenty Twenty fork that strives to strike a balance between options and opinions. Optimized for modern browsers.

Notable differences from Twenty Twenty:

- No edit post links embedded on front-end. The admin bar already provides an edit link, making these redundant. Removing them improves consistency between how the website looks logged in versus logged out.
- The default body and heading font is now Nunito.
- Support for older browser, PHP, and WP versions have been dropped, allowing the code to be shrunk down and optimized for modern browsers.
- The codebase uses PHP namespaces.

Planned future changes:

- Better `cite` styling outside of `blockquote` elements.
- Sticky header option.
- Custom font support.
- Customizable font scale.

== Changelog ==

Not released yet.

= 0.0 =
* Released: January __, 2020

Initial release

== Copyright ==

Zerenity WordPress Theme, © 2019-2020 Zebulan Stanphill

Zerenity is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/gpl-3.0.html.

Zerenity is derived from Twenty Twenty, © 2019-2020 WordPress.org

Twenty Twenty is distributed under GPLv2 or later: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Source: https://github.com/WordPress/twentytwenty and https://github.com/WordPress/WordPress/tree/master/wp-content/themes/twentytwenty (latest version)

Twenty Twenty itself is derived from the Chaplin Theme, © 2019 Anders Norén

Chaplin Theme is distributed under GPLv2 or later: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Source: https://wordpress.org/themes/chaplin/

Zerenity bundles the following third-party resources:

Illustrations in screenshot.png by Tammie Lister
License: Creative Commons Zero (CC0), https://creativecommons.org/publicdomain/zero/1.0/

Nunito font
© 2014 Vernon Adams (vern@newtypography.co.uk)
License: SIL Open Font License, 1.1, https://opensource.org/licenses/OFL-1.1
Source: https://github.com/vernnobile/NunitoFont

Bespoke Icons Created For Twenty Twenty
License: Creative Commons Zero (CC0), https://creativecommons.org/publicdomain/zero/1.0/
List of bespoke icons:
- Search icon
- Menu icon

Feather Icons
© 2013-2017 Cole Bemis
License: MIT License, https://opensource.org/licenses/MIT
Source: https://feathericons.com
Used for post meta icons, and the link icon in the social menu.

Social Icons
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Source: WordPress Social Link Block (See wp-includes\blocks\social-link.php)

Code from Twenty Nineteen
© 2018-2019 WordPress.org
License: GPLv2 or later
Source: https://wordpress.org/themes/twentynineteen/
Included as part of the following classes and functions:
- Zebulan\Zerenity\SVG_Icons
- Zebulan\Zerenity\the_theme_svg()
- Zebulan\Zerenity\get_theme_svg()
- Zebulan\Zerenity\nav_menu_social_icons()

Underscores 
© 2012-2019 Automattic, Inc.
License: [GPLv2 or later](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
Source: https://underscores.me/
