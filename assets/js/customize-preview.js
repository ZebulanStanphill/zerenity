/* global zerenityBgColors, zerenityPreviewEls, jQuery, _, wp */
/**
 * Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since 1.0
 */

;(function($, api, _) {
	/**
	 * Return a value for our partial refresh.
	 *
	 * @param {Object} partial  Current partial.
	 *
	 * @return {jQuery.Promise} Resolved promise.
	 */
	function returnDeferred(partial) {
		const deferred = new $.Deferred()

		deferred.resolveWith(
			partial,
			partial.placements().map(() => '')
		)

		return deferred.promise()
	}

	// Selective refresh for "Fixed Background Image".
	api.selectiveRefresh.partialConstructor.cover_fixed = api.selectiveRefresh.Partial.extend(
		{
			/**
			 * Override the refresh method.
			 *
			 * @return {jQuery.Promise} Resolved promise.
			 */
			refresh() {
				const partial = this
				const params = partial.params
				const covers = document.querySelectorAll(params.selector)

				if (covers.length) {
					for (const cover of covers) {
						if (cover.classList.contains('bg-image')) {
							cover.classList.toggle('bg-attachment-fixed')
						}
					}
				}

				return returnDeferred(partial)
			}
		}
	)

	// Selective refresh for "Image Overlay Opacity".
	api.selectiveRefresh.partialConstructor.cover_opacity = api.selectiveRefresh.Partial.extend(
		{
			/**
			 * Input attributes.
			 *
			 * @type {Object}
			 */
			attrs: {},

			/**
			 * Override the refresh method.
			 *
			 * @return {jQuery.Promise} Resolved promise.
			 */
			refresh() {
				const partial = this
				const params = partial.params
				const covers = document.querySelectorAll(params.selector)

				if (covers.length) {
					const attrs = partial.attrs
					const ranges = _.range(attrs.min, attrs.max + attrs.step, attrs.step)
					const setting = api(params.primarySetting)
					const classNames = ranges.map(val => `opacity-${val}`)
					const className =
						classNames[ranges.indexOf(parseInt(setting.get(), 10))]

					for (const cover of covers) {
						cover.classList.remove(...classNames)
						cover.classList.add(className)
					}
				}

				return returnDeferred(partial)
			}
		}
	)

	// Add listener for the "header_footer_background_color" control.
	api('header_footer_background_color', function(value) {
		value.bind(function(to) {
			// Add background color to header and footer wrappers.
			document.querySelector(
				'body:not(.overlay-header)#site-header, #site-footer'
			).style.backgroundColor = to

			// Change body classes if this is the same background-color as the content background.
			if (
				to.toLowerCase() ===
				api('background_color')
					.get()
					.toLowerCase()
			) {
				document.body.classList.add('reduced-spacing')
			} else {
				document.body.classList.remove('reduced-spacing')
			}
		})
	})

	// Add listener for the "background_color" control.
	api('background_color', function(value) {
		value.bind(function(to) {
			// Change body classes if this is the same background-color as the header/footer background.
			if (
				to.toLowerCase() ===
				api('header_footer_background_color')
					.get()
					.toLowerCase()
			) {
				document.body.classList.add('reduced-spacing')
			} else {
				document.body.classList.remove('reduced-spacing')
			}
		})
	})

	// Add listener for the accent color.
	api('accent_hue', function(value) {
		value.bind(function() {
			// Generate the styles.
			// Add a small delay to be sure the accessible colors were generated.
			setTimeout(function() {
				for (const context of Object.keys(zerenityBgColors)) {
					zerenityGenerateColorA11yPreviewStyles(context)
				}
			}, 50)
		})
	})

	// Add listeners for background-color settings.
	for (const context of Object.keys(zerenityBgColors)) {
		wp.customize(zerenityBgColors[context].setting, function(value) {
			value.bind(function() {
				// Generate the styles.
				// Add a small delay to be sure the accessible colors were generated.
				setTimeout(function() {
					zerenityGenerateColorA11yPreviewStyles(context)
				}, 50)
			})
		})
	}

	/**
	 * Add styles to elements in the preview pane.
	 *
	 * @since 1.0
	 *
	 * @param {string} context The area for which we want to generate styles. Can be for example "content", "header" etc.
	 *
	 * @return {void}
	 */
	function zerenityGenerateColorA11yPreviewStyles(context) {
		// Get the accessible colors option.
		const a11yColors = window.parent.wp
				.customize('accent_accessible_colors')
				.get(),
			stylesheetID = 'zerenity-customizer-styles-' + context

		let stylesheet = document.getElementById(stylesheetID)

		// If the stylesheet doesn't exist, create it and append it to <head>.
		if (stylesheet === null) {
			const inlineStyleEl = document.getElementById('zerenity-style-inline-css')

			// Create a new style element.
			const customizerStylesEl = document.createElement('style')

			customizerStylesEl.id = stylesheetID

			// Append it after inlineStyleEl.
			inlineStyleEl.parentNode.insertBefore(
				customizerStylesEl,
				inlineStyleEl.nextSibling
			)
			stylesheet = document.getElementById(stylesheetID)
		}

		let styles = ''

		if (a11yColors[context] !== undefined) {
			// Check if we have elements defined.
			if (zerenityPreviewEls[context]) {
				for (const [setting, items] of Object.entries(
					zerenityPreviewEls[context]
				)) {
					for (const [property, elements] of Object.entries(items)) {
						if (a11yColors[context][setting] !== undefined) {
							styles +=
								elements.join(',') +
								'{' +
								property +
								':' +
								a11yColors[context][setting] +
								';}'
						}
					}
				}
			}
		}
		// Add styles.
		stylesheet.innerHTML = styles
	}
	// Generate styles on load. Handles page-changes on the preview pane.
	wp.domReady(function() {
		zerenityGenerateColorA11yPreviewStyles('content')
		zerenityGenerateColorA11yPreviewStyles('header-footer')
	})
})(jQuery, wp.customize, _)
