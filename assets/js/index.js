/*	-----------------------------------------------------------------------------------------------
	Namespace
--------------------------------------------------------------------------------------------------- */
const zerenity = window.zerenity || {}

// Set a default value for scrolled.
zerenity.scrolled = 0

/*	-----------------------------------------------------------------------------------------------
	Cover Modals
--------------------------------------------------------------------------------------------------- */

zerenity.coverModals = {
	init() {
		if (document.getElementsByClassName('cover-modal').length) {
			// Handle cover modals when they're toggled.
			this.onToggle()

			// When toggled, untoggle if visitor clicks on the wrapping element of the modal.
			this.outsideUntoggle()

			// Close on escape key press.
			this.closeOnEscape()

			// Hide and show modals before and after their animations have played out.
			this.hideAndShowModals()
		}
	},

	// Handle cover modals when they're toggled.
	onToggle() {
		for (const element of document.getElementsByClassName('cover-modal')) {
			element.addEventListener('toggled', function(event) {
				const modal = event.target,
					body = document.body

				if (modal.classList.contains('active')) {
					body.classList.add('showing-modal')
				} else {
					body.classList.remove('showing-modal')
					body.classList.add('hiding-modal')

					// Remove the hiding class after a delay, when animations have been run.
					setTimeout(function() {
						body.classList.remove('hiding-modal')
					}, 500)
				}
			})
		}
	},

	// Close modal on outside click.
	outsideUntoggle() {
		document.addEventListener(
			'click',
			function(event) {
				const target = event.target
				const modal = document.getElementsByClassName('cover-modal active')[0]

				if (target === modal) {
					this.untoggleModal(target)
				}
			}.bind(this)
		)
	},

	// Close modal on escape key press.
	closeOnEscape() {
		document.addEventListener(
			'keydown',
			function(event) {
				if (event.keyCode === 27) {
					event.preventDefault()
					for (const element of document.getElementsByClassName(
						'cover-modal active'
					)) {
						this.untoggleModal(element)
					}
				}
			}.bind(this)
		)
	},

	// Hide and show modals before and after their animations have played out.
	hideAndShowModals() {
		const _doc = document,
			_win = window,
			modals = _doc.getElementsByClassName('cover-modal'),
			htmlStyle = _doc.documentElement.style,
			adminBar = _doc.getElementById('wpadminbar')

		function getAdminBarHeight(negativeValue) {
			const currentScroll = _win.pageYOffset

			if (adminBar) {
				const height = currentScroll + adminBar.getBoundingClientRect().height

				return negativeValue ? -height : height
			}

			return currentScroll === 0 ? 0 : -currentScroll
		}

		function htmlStyles() {
			const overflow =
				_win.innerHeight > _doc.documentElement.getBoundingClientRect().height

			return {
				'overflow-y': overflow ? 'hidden' : 'scroll',
				position: 'fixed',
				width: '100%',
				top: getAdminBarHeight(true) + 'px',
				left: 0
			}
		}

		// Show the modal.
		for (const modal of modals) {
			modal.addEventListener('toggle-target-before-inactive', function(event) {
				const offsetY = _win.pageYOffset,
					paddingTop = Math.abs(getAdminBarHeight()) - offsetY + 'px'

				if (event.target !== modal) {
					return
				}

				const styles = htmlStyles()

				for (const [styleKey, style] of Object.entries(styles)) {
					htmlStyle.setProperty(styleKey, style)
				}

				zerenity.scrolled = parseInt(styles.top, 10)

				if (adminBar) {
					_doc.body.style.setProperty('padding-top', paddingTop)

					const mQuery = _win.matchMedia('(max-width: 600px)')

					if (mQuery.matches) {
						if (offsetY >= getAdminBarHeight()) {
							modal.style.setProperty('top', 0)
						} else {
							modal.style.setProperty(
								'top',
								getAdminBarHeight() - offsetY + 'px'
							)
						}
					}
				}

				modal.classList.add('show-modal')
			})

			// Hide the modal after a delay, so animations have time to play out.
			modal.addEventListener('toggle-target-after-inactive', function(event) {
				if (event.target !== modal) {
					return
				}

				setTimeout(function() {
					let clickedEl = zerenity.toggles.clickedEl

					modal.classList.remove('show-modal')

					for (const styleKey of Object.keys(htmlStyles())) {
						htmlStyle.removeProperty(styleKey)
					}

					if (adminBar) {
						_doc.body.style.removeProperty('padding-top')
						modal.style.removeProperty('top')
					}

					if (clickedEl !== false) {
						clickedEl.focus()
						clickedEl = false
					}

					_win.scrollTo(0, Math.abs(zerenity.scrolled + getAdminBarHeight()))

					zerenity.scrolled = 0
				}, 500)
			})
		}
	},

	// Untoggle a modal.
	untoggleModal(modal) {
		let modalTargetClass,
			modalToggle = false

		// If the modal has specified the string (ID or class) used by toggles to target it, untoggle the toggles with that target string.
		// The modal-target-string must match the string toggles use to target the modal.
		if (modal.dataset.modalTargetString) {
			modalTargetClass = modal.dataset.modalTargetString

			modalToggle = document.querySelector(
				`*[data-toggle-target="${modalTargetClass}"]`
			)
		}

		// If a modal toggle exists, trigger it so all of the toggle options are included.
		if (modalToggle) {
			modalToggle.click()

			// If one doesn't exist, just hide the modal.
		} else {
			modal.classList.remove('active')
		}
	}
} // zerenity.coverModals

/*	-----------------------------------------------------------------------------------------------
	Intrinsic Ratio Embeds
--------------------------------------------------------------------------------------------------- */

zerenity.intrinsicRatioVideos = {
	init() {
		this.makeFit()

		window.addEventListener(
			'resize',
			function() {
				this.makeFit()
			}.bind(this)
		)
	},

	makeFit() {
		for (const video of document.querySelectorAll('iframe, object, video')) {
			const container = video.parentNode

			// Skip videos we want to ignore.
			if (
				video.classList.contains('intrinsic-ignore') ||
				video.parentNode.classList.contains('intrinsic-ignore')
			) {
				return true
			}

			if (!video.dataset.origwidth) {
				// Get the video element proportions.
				video.setAttribute('data-origwidth', video.width)
				video.setAttribute('data-origheight', video.height)
			}

			const iTargetWidth = container.offsetWidth

			// Get ratio from proportions.
			const ratio = iTargetWidth / video.dataset.origwidth

			// Scale based on ratio, thus retaining proportions.
			video.style.width = iTargetWidth + 'px'
			video.style.height = video.dataset.origheight * ratio + 'px'
		}
	}
} // zerenity.instrinsicRatioVideos

/*	-----------------------------------------------------------------------------------------------
	Modal Menu
--------------------------------------------------------------------------------------------------- */
zerenity.modalMenu = {
	init() {
		// If the current menu item is in a sub level, expand all the levels higher up on load.
		this.expandLevel()
		this.keepFocusInModal()
	},

	expandLevel() {
		const modalMenus = document.getElementsByClassName('modal-menu')

		for (const modalMenu of modalMenus) {
			const activeMenuItem = modalMenu.getElementsByClassName(
				'current-menu-item'
			)[0]

			if (activeMenuItem) {
				for (const element of zerenityFindParents(activeMenuItem, 'li')) {
					const subMenuToggle = element.getElementsByClassName(
						'sub-menu-toggle'
					)[0]
					if (subMenuToggle) {
						zerenity.toggles.performToggle(subMenuToggle, true)
					}
				}
			}
		}
	},

	keepFocusInModal() {
		const _doc = document

		_doc.addEventListener('keydown', function(event) {
			let toggleTarget,
				modal,
				selectors,
				elements,
				menuType,
				bottomMenu,
				activeEl,
				lastEl,
				firstEl,
				tabKey,
				shiftKey

			const clickedEl = zerenity.toggles.clickedEl

			if (clickedEl && _doc.body.classList.contains('showing-modal')) {
				toggleTarget = clickedEl.dataset.toggleTarget
				selectors = 'input, a, button'
				modal = _doc.querySelector(toggleTarget)

				elements = modal.querySelectorAll(selectors)
				elements = Array.prototype.slice.call(elements)

				if ('.menu-modal' === toggleTarget) {
					menuType = window.matchMedia('(min-width: 1000px)').matches
					menuType = menuType ? '.expanded-menu' : '.mobile-menu'

					elements = elements.filter(function(element) {
						return (
							null !== element.closest(menuType) &&
							null !== element.offsetParent
						)
					})

					elements.unshift(_doc.getElementsByClassName('close-nav-toggle')[0])

					bottomMenu = _doc.querySelector('.menu-bottom > nav')

					if (bottomMenu) {
						for (const element of bottomMenu.querySelectorAll(selectors)) {
							elements.push(element)
						}
					}
				}

				lastEl = elements[elements.length - 1]
				firstEl = elements[0]
				activeEl = _doc.activeElement
				tabKey = event.keyCode === 9
				shiftKey = event.shiftKey

				if (!shiftKey && tabKey && lastEl === activeEl) {
					event.preventDefault()
					firstEl.focus()
				}

				if (shiftKey && tabKey && firstEl === activeEl) {
					event.preventDefault()
					lastEl.focus()
				}
			}
		})
	}
} // zerenity.modalMenu

/*	-----------------------------------------------------------------------------------------------
	Primary Menu
--------------------------------------------------------------------------------------------------- */

zerenity.primaryMenu = {
	init() {
		this.focusMenuWithChildren()
	},

	// The focusMenuWithChildren() function implements Keyboard Navigation in the Primary Menu
	// by adding the '.focus' class to all 'li.menu-item-has-children' when the focus is on the 'a' element.
	focusMenuWithChildren() {
		// Get all the link elements within the primary menu.
		const menu = document.getElementsByClassName('primary-menu-wrapper')[0]

		if (!menu) {
			return false
		}

		const links = menu.getElementsByTagName('a')

		// Each time a menu link is focused or blurred, toggle focus.
		for (const link of links) {
			link.addEventListener('focus', toggleFocus, true)
			link.addEventListener('blur', toggleFocus, true)
		}

		// Sets or removes the .focus class on an element.
		function toggleFocus() {
			let self = this

			// Move up through the ancestors of the current link until we hit .primary-menu.
			while (!self.classList.contains('primary-menu')) {
				// On li elements toggle the class .focus.
				if (self.tagName.toLowerCase() === 'li') {
					if (self.classList.contains('focus')) {
						self.classList.remove('focus')
					} else {
						self.classList.add('focus')
					}
				}
				self = self.parentElement
			}
		}
	}
} // zerenity.primaryMenu

/*	-----------------------------------------------------------------------------------------------
	Toggles
--------------------------------------------------------------------------------------------------- */

zerenity.toggles = {
	clickedEl: false,

	init() {
		// Do the toggle.
		this.toggle()

		// Check for toggle/untoggle on resize.
		this.resizeCheck()

		// Check for untoggle on escape key press.
		this.untoggleOnEscapeKeyPress()
	},

	performToggle(element, instantly) {
		let target, timeOutTime

		const self = this,
			_doc = document,
			// Get our targets.
			toggle = element,
			targetString = toggle.dataset.toggleTarget,
			activeClass = 'active'

		// Elements to focus after modals are closed.
		if (!_doc.getElementsByClassName('show-modal').length) {
			self.clickedEl = _doc.activeElement
		}

		if (targetString === 'next') {
			target = toggle.nextSibling
		} else {
			target = _doc.querySelector(targetString)
		}

		// Trigger events on the toggle targets before they are toggled.
		if (target.classList.contains(activeClass)) {
			target.dispatchEvent(new Event('toggle-target-before-active'))
		} else {
			target.dispatchEvent(new Event('toggle-target-before-inactive'))
		}

		// Get the class to toggle, if specified.
		const classToToggle = toggle.dataset.classToToggle
			? toggle.dataset.classToToggle
			: activeClass

		// For cover modals, set a short timeout duration so the class animations have time to play out.
		timeOutTime = 0

		if (target.classList.contains('cover-modal')) {
			timeOutTime = 10
		}

		setTimeout(function() {
			let focusElement

			const subMenued = target.classList.contains('sub-menu'),
				newTarget = subMenued
					? toggle.closest('.menu-item').getElementsByClassName('sub-menu')[0]
					: target,
				duration = toggle.dataset.toggleDuration

			// Toggle the target of the clicked toggle.
			if (
				toggle.dataset.toggleType === 'slidetoggle' &&
				!instantly &&
				duration !== '0'
			) {
				zerenityMenuToggle(newTarget, duration)
			} else {
				newTarget.classList.toggle(classToToggle)
			}

			// If the toggle target is 'next', only give the clicked toggle the active class.
			if (targetString === 'next') {
				toggle.classList.toggle(activeClass)
			} else if (target.classList.contains('sub-menu')) {
				toggle.classList.toggle(activeClass)
			} else {
				// If not, toggle all toggles with this toggle target.
				_doc
					.querySelector(`*[data-toggle-target="${targetString}"]`)
					.classList.toggle(activeClass)
			}

			// Toggle aria-expanded on the toggle.
			zerenityToggleAttribute(toggle, 'aria-expanded', 'true', 'false')

			if (
				self.clickedEl &&
				-1 !== toggle.getAttribute('class').indexOf('close-')
			) {
				zerenityToggleAttribute(
					self.clickedEl,
					'aria-expanded',
					'true',
					'false'
				)
			}

			// Toggle body class.
			if (toggle.dataset.toggleBodyClass) {
				_doc.body.classList.toggle(toggle.dataset.toggleBodyClass)
			}

			// Check whether to set focus.
			if (toggle.dataset.setFocus) {
				focusElement = _doc.querySelector(toggle.dataset.setFocus)

				if (focusElement) {
					if (target.classList.contains(activeClass)) {
						focusElement.focus()
					} else {
						focusElement.blur()
					}
				}
			}

			// Trigger the toggled event on the toggle target.
			target.dispatchEvent(new Event('toggled'))

			// Trigger events on the toggle targets after they are toggled.
			if (target.classList.contains(activeClass)) {
				target.dispatchEvent(new Event('toggle-target-after-active'))
			} else {
				target.dispatchEvent(new Event('toggle-target-after-inactive'))
			}
		}, timeOutTime)
	},

	// Do the toggle.
	toggle() {
		const self = this

		for (const element of document.querySelectorAll('*[data-toggle-target]')) {
			element.addEventListener('click', function(event) {
				event.preventDefault()
				self.performToggle(element)
			})
		}
	},

	// Check for toggle/untoggle on screen resize.
	resizeCheck() {
		if (
			document.querySelectorAll(
				'*[data-untoggle-above], *[data-untoggle-below], *[data-toggle-above], *[data-toggle-below]'
			).length
		) {
			window.addEventListener('resize', function() {
				const winWidth = window.innerWidth,
					toggles = document.getElementsByClassName('toggle')

				for (const toggle of toggles) {
					const unToggleAbove = toggle.dataset.untoggleAbove,
						unToggleBelow = toggle.dataset.untoggleBelow,
						toggleAbove = toggle.dataset.toggleAbove,
						toggleBelow = toggle.dataset.toggleBelow

					// If no width comparison is set, continue.
					if (
						!unToggleAbove &&
						!unToggleBelow &&
						!toggleAbove &&
						!toggleBelow
					) {
						return
					}

					// If the toggle width comparison is true, toggle the toggle.
					if (
						(((unToggleAbove && winWidth > unToggleAbove) ||
							(unToggleBelow && winWidth < unToggleBelow)) &&
							toggle.classList.contains('active')) ||
						(((toggleAbove && winWidth > toggleAbove) ||
							(toggleBelow && winWidth < toggleBelow)) &&
							!toggle.classList.contains('active'))
					) {
						toggle.click()
					}
				}
			})
		}
	},

	// Close toggle on escape key press.
	untoggleOnEscapeKeyPress() {
		document.addEventListener('keyup', function(event) {
			if (event.key === 'Escape') {
				for (const element of document.querySelectorAll(
					'*[data-untoggle-on-escape].active'
				)) {
					if (element.classList.contains('active')) {
						element.click()
					}
				}
			}
		})
	}
} // zerenity.toggles

/**
 * Is the DOM ready?
 *
 * This implementation is coming from https://gomakethings.com/a-native-javascript-equivalent-of-jquerys-ready-method/
 *
 * @param {Function} fn Callback function to run.
 */
function zerenityDomReady(fn) {
	if (typeof fn !== 'function') {
		return
	}

	if (
		document.readyState === 'interactive' ||
		document.readyState === 'complete'
	) {
		return fn()
	}

	document.addEventListener('DOMContentLoaded', fn, false)
}

zerenityDomReady(function() {
	zerenity.toggles.init() // Handle toggles.
	zerenity.coverModals.init() // Handle cover modals.
	zerenity.intrinsicRatioVideos.init() // Retain aspect ratio of videos on window resize.
	zerenity.modalMenu.init() // Modal Menu.
	zerenity.primaryMenu.init() // Primary Menu.
})

/*	-----------------------------------------------------------------------------------------------
	Helper functions
--------------------------------------------------------------------------------------------------- */

/* Toggle an attribute ----------------------- */

function zerenityToggleAttribute(element, attribute, trueVal, falseVal) {
	if (trueVal === undefined) {
		trueVal = true
	}
	if (falseVal === undefined) {
		falseVal = false
	}
	if (element.getAttribute(attribute) !== trueVal) {
		element.setAttribute(attribute, trueVal)
	} else {
		element.setAttribute(attribute, falseVal)
	}
}

/**
 * Toggle a menu item on or off.
 *
 * @param {HTMLElement} target
 * @param {number} duration
 */
function zerenityMenuToggle(target, duration) {
	let transitionListener

	const initialPositions = [],
		finalPositions = []

	if (!target) {
		return
	}

	const menu = target.closest('.menu-wrapper')

	// Step 1: look at the initial positions of every menu item.
	// We need to convert the HTMLCollection to an array for the for-of loop.
	const menuItems = [...menu.getElementsByClassName('menu-item')]

	for (const [index, menuItem] of menuItems.entries()) {
		initialPositions[index] = { x: menuItem.offsetLeft, y: menuItem.offsetTop }
	}

	const initialParentHeight = target.parentElement.offsetHeight

	target.classList.add('toggling-target')

	// Step 2: toggle target menu item and look at the final positions of every menu item.
	target.classList.toggle('active')

	for (const [index, menuItem] of menuItems.entries()) {
		finalPositions[index] = { x: menuItem.offsetLeft, y: menuItem.offsetTop }
	}

	const finalParentHeight = target.parentElement.offsetHeight

	// Step 3: close target menu item again.
	// The whole process happens without giving the browser a chance to render, so it's invisible.
	target.classList.toggle('active')

	// Step 4: prepare animation.
	// Position all the items with absolute offsets, at the same starting position.
	// Shouldn't result in any visual changes if done right.
	menu.classList.add('is-toggling')
	target.classList.toggle('active')
	for (const [index, menuItem] of menuItems.entries()) {
		const initialPosition = initialPositions[index]
		if (initialPosition.y === 0 && menuItem.parentElement === target) {
			initialPosition.y = initialParentHeight
		}
		menuItem.style.transform = `translate(${initialPosition.x}px, ${initialPosition.y}px)`
	}

	// The double rAF is unfortunately needed, since we're toggling CSS classes, and
	// the only way to ensure layout completion here across browsers is to wait twice.
	// This just delays the start of the animation by 2 frames and is thus not an issue.
	requestAnimationFrame(function() {
		requestAnimationFrame(function() {
			// Step 5: start animation by moving everything to final position.
			// All the layout work has already happened, while we were preparing for the animation.
			// The animation now runs entirely in CSS, using cheap CSS properties (opacity and transform)
			// that don't trigger the layout or paint stages.
			menu.classList.add('is-animating')
			for (const [index, menuItem] of menuItems.entries()) {
				const finalPosition = finalPositions[index]
				if (finalPosition.y === 0 && menuItem.parentElement === target) {
					finalPosition.y = finalParentHeight
				}
				if (duration !== undefined) {
					menuItem.style.transitionDuration = duration + 'ms'
				}
				menuItem.style.transform = `translate(${finalPosition.x}px, ${finalPosition.y}px)`
			}
			if (duration !== undefined) {
				target.style.transitionDuration = duration + 'ms'
			}
		})

		// Step 6: finish toggling.
		// Remove all transient classes when the animation ends.
		transitionListener = function() {
			menu.classList.remove('is-animating')
			menu.classList.remove('is-toggling')
			target.classList.remove('toggling-target')
			for (const menuItem of menuItems) {
				menuItem.style.transform = ''
				menuItem.style.transitionDuration = ''
			}
			target.style.transitionDuration = ''
			target.removeEventListener('transitionend', transitionListener)
		}

		target.addEventListener('transitionend', transitionListener)
	})
}

/**
 * Traverses the DOM up to find elements matching the query.
 *
 * @param {HTMLElement} target
 * @param {string} query
 * @return {NodeList} parents matching query
 */
function zerenityFindParents(target, query) {
	const parents = []

	// Recursively go up the DOM adding matches to the parents array.
	function traverse(item) {
		const parent = item.parentNode
		if (parent instanceof HTMLElement) {
			if (parent.matches(query)) {
				parents.push(parent)
			}
			traverse(parent)
		}
	}

	traverse(target)

	return parents
}
