<?php
/**
 * Customizer Separator Control settings for this theme.
 *
 * @package Zebulan\Zerenity
 * @since 1.0
 */

namespace Zebulan\Zerenity;

if ( class_exists( 'WP_Customize_Control' ) ) {

	if ( ! class_exists( __NAMESPACE__ . '\Separator_Control' ) ) {
		/**
		 * Separator Control.
		 */
		class Separator_Control extends \WP_Customize_Control {
			/**
			 * Render the hr.
			 */
			public function render_content(): void {
				echo '<hr/>';
			}

		}
	}
}
