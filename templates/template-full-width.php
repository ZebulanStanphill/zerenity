<?php
/**
 * Template Name: Full Width Template
 * Template Post Type: post, page
 *
 * @package Zebulan\Zerenity
 * @since 1.0
 */

namespace Zebulan\Zerenity;

get_template_part( 'singular' );
